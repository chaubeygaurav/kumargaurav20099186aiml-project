#!/usr/bin/env python
# coding: utf-8

# ## Melbourne Housing Price 

# #### Importing the necessary libraries

# In[1]:


import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv("Melbourne_housing_FULL.csv (1).zip")
data.head(5)


# In[3]:


data.describe()


# In[4]:


data.info()


# In[5]:


data.isnull().sum()


# In[6]:


data.nunique()


# ### Dealing with the missing values

# In[7]:


# Now we will be dealing with the missing values. As we can see from the above dataset Postcode and Distance has 1 missing rows.
#No we will be dealing with the missing values of the price.
data = data[data.Postcode.notnull()]


# In[8]:


print(data)


# In[9]:


data = data[data.Price.notnull()]


# In[10]:


data.isnull().sum()


# In[11]:


sns.distplot(data['Price'])


# In[12]:


facet = sns.lmplot(data=data, x='Bedroom2', y='Price', hue='Type', 
                   fit_reg=False, legend=True, legend_out=True)
plt.show()


# In[13]:


data.loc[data.Bathroom.isnull(), 'Bathroom']  = data.groupby('Rooms')['Bathroom'].transform(lambda x: x.fillna(x.mean()))
data.head()


# In[14]:


data.loc[data.Car.isnull(), 'Car']  = data.groupby('Type')['Car'].transform(lambda x: x.fillna(x.mean()))
data.loc[data.Landsize.isnull(), 'Landsize']  = data.groupby('Type')['Landsize'].transform(lambda x: x.fillna(x.mean()))


# In[15]:


data.isnull().sum()


# In[16]:


# Council Area,Regionname, Property count , here 2 of the rows are missing . Well we cant determine that. 
data=data[data.Regionname.notnull()]


# In[17]:


# Here we will be observing the prices of the different types 
sns.factorplot(x="Type", y="Price", hue = "Method", data= data, kind = "bar")


# ##### From the above factorplot it is quite clear that h type has higher prices compared to the rest two types.

# In[18]:


plt.figure(figsize=(14,9), facecolor='w', edgecolor='k')
sns.violinplot(data['Regionname'], data['Price'])
plt.title(" Regionname vs Price")
plt.xticks(rotation=60)


# ###### Here its very very clear from this violinplot that Southern Metropolitan is the most expensive region and has wider price region  and  Western Victoria is the cheapest region.

# In[19]:


data.groupby('Regionname', as_index=False).agg({"Price": "mean"})


# In[20]:


facet = sns.lmplot(data=data, x='Rooms', y='Price', fit_reg=False, legend=True, legend_out=True)


# H type has higher price range between 4 to 6 rooms. 

# In[21]:


data.groupby('Regionname')[['SellerG']].count()


# In[22]:


data.groupby('Regionname', as_index=False).agg({"Car": "mean","Price": "mean","Rooms": "mean"})


# In[23]:


data.groupby(['Regionname']).agg({ 'SellerG': "count", 'Rooms':"mean"})  


# In[24]:


data=data[data.Bathroom.notnull()]
data['Bathroom'] = data['Bathroom'].apply(lambda x: round(x))
data['Car'] = data['Car'].apply(lambda x: round(x))


# In[25]:


data.isnull().sum()


# Here we will be doing the Feature Engineering for more accuracy and the reliability.
# converting the categorical data into numerical data.
# using dummies.

# In[26]:


data = pd.concat([data, pd.get_dummies(data["Type"]), pd.get_dummies(data["Method"]), pd.get_dummies(data["Regionname"])], axis=1)
data = data.drop(["Suburb", "Address", "SellerG", "CouncilArea", "Type", "Method", "Regionname"], 1)
data['Date'] = [pd.Timestamp(x).timestamp() for x in data["Date"]]
data = data.dropna()
data.head()


# In[ ]:




